#!/bin/sh

if [ -z "$BUCKET_NAME" ]; then
  echo Need a BUCKET_NAME
  exit 1
fi

LAST_UPLOAD=$(aws s3api list-objects-v2 --bucket $BUCKET_NAME --query 'reverse(sort_by(Contents, &LastModified))[:1].Key' --output=text)

if [ ! -z "$1" ]; then
  echo "Looking for: $1"
  LAST_UPLOAD=$(aws s3api list-objects-v2 --bucket $BUCKET_NAME --query "Contents[?contains(Key, \`$1\`)].Key" --output=text)
  echo "Found: $LAST_UPLOAD"
fi

if [ -z "$LAST_UPLOAD" ]; then
  echo No backups found
  exit 1
fi

if [ ! -e /data/$LAST_UPLOAD ]; then
	echo "Downloading $LAST_UPLOAD"
	aws s3 cp s3://$BUCKET_NAME/$LAST_UPLOAD /data
else
	echo $LAST_UPLOAD already present
fi

echo Extracing $LAST_UPLOAD

cd /data
tar zxf $LAST_UPLOAD --strip-components=1

echo Removing archive
rm /data/$LAST_UPLOAD
echo Restore complete
