# Docker S3 Cron Backup

All credit to [:star: Github](https://github.com/peterrus/docker-s3-cron-backup), I have completely plagiarised the code.

## What is it?
Slim container that runs a s3 backup by tar gzipping /data to a s3 bucket (on entrypoint not cron)

## Now, how do I use it?
The container is configured via a set of environment variables:
- AWS_ACCESS_KEY: Get this from amazon IAM
- AWS_SECRET_ACCESS_KEY: Get this from amazon IAM, **you should keep this a secret**
- S3_BUCKET_URL: in most cases this should be s3://name-of-your-bucket/
- AWS_DEFAULT_REGION: The AWS region your bucket resides in
- BACKUP_NAME: A name to identify your backup among the other files in your bucket, it will be postfixed with the current timestamp (date and time)

All environment variables prefixed with 'AWS_' are directly used by [awscli](https://aws.amazon.com/cli/) that this image heavily relies on.
