FROM python:3.8-alpine

RUN pip install awscli \
  && apk add tar

COPY *.sh /

RUN chmod +x /*.sh
