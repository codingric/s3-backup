#!/bin/sh
if [ -z "$BACKUP_NAME" ]; then
  echo Need a BACKUP_NAME
  exit 1
fi

if [ -z "$BUCKET_NAME" ]; then
  echo Need a BUCKET_NAME
  exit 1
fi

echo "Creating archive..."
tar -zcvf /tmp/$BACKUP_NAME-`date "+%Y-%m-%d_%H-%M-%S"`.tar.gz /data
echo "Uploading archive to S3..."
aws s3 cp /tmp/*.tar.gz s3://$BUCKET_NAME/
echo "Removing local archive..."
rm /tmp/*.tar.gz
echo "Backup complete"
